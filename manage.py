# coding=utf-8

from getpass import getpass
from flask_script import Manager
from passlib.hash import pbkdf2_sha256
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.db.models import Base, User
from src.bot.telekuponbot import bot
from src.web.webapp import app

engine = create_engine('sqlite:///dev.db')
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

manager = Manager(app)


@manager.command
@manager.option('-u', '--username', help='Имя пользователя')
def createsuperuser(username):
    password = getpass()
    hash = pbkdf2_sha256.encrypt(password, rounds=200000, salt_size=16)
    session.add(User(username=username, password=hash))
    session.commit()


@manager.command
def startbot():
    bot.polling(none_stop=True)


@manager.command
def startserver():
    app.run(host='0.0.0.0', port=80)

if __name__ == '__main__':
    manager.run()