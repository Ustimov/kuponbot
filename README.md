# kuponbot
Technopark project. Telegram bot for free sales kupon on Python

[![Build Status](https://travis-ci.org/Ustimov/kuponbot.svg?branch=master)](https://travis-ci.org/Ustimov/kuponbot)

## Getting Started

Setup environment:

```
git clone https://github.com/mmarashan/kuponbot.git
cd kuponbot/
virtualenv .
source bin/activate
pip3 install -r requirements.txt
```

Insert actual bot token here:

```
updater = Updater('TOKEN HERE')
```

Run the bot:

```
python3 kuponbot.py
```

## Run tests

```
python3 -m unittest
```

## Run admin panel

```
python3 webapp.py
```

## Deploy with Fabric

```
fab -H username@hostname deploy
```