from fabric.api import cd, sudo

app_dir = '/webapps/kuponbot/kuponbot'


def deploy():
    pull()
    restart()


def pull():
    with cd(app_dir):
        sudo('git pull')


def restart():
    sudo('supervisorctl restart webapp')
    sudo('supervisorctl restart kuponbot')
