from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship
from flask_login.mixins import UserMixin

Base = declarative_base()


class Address(Base):
    __tablename__ = "addresses"

    id = Column(Integer, primary_key=True)
    metro_station = Column(String(255))
    # район. Например: Басманный
    distinct = Column(String(255))
    # полный адрес начиная с улицы
    full_adress = Column(String(255))


class Company(Base):
    __tablename__ = 'companies'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    address_id = Column(Integer, ForeignKey('addresses.id'))
    address = relationship(Address)


class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)


class Coupon(Base):
    __tablename__ = 'coupons'

    id = Column(Integer, primary_key=True)
    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship(Category)
    company_id = Column(Integer, ForeignKey('companies.id'))
    company = relationship(Company)
    code = Column(String(255), nullable=False)
    description = Column(String(1000), nullable=False)
    min_price = Column(Integer)
    start_datetime = Column(DateTime)
    expire_datetime = Column(DateTime)


class User(Base, UserMixin):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(32), nullable=False)
    password = Column(String(88), nullable=False)