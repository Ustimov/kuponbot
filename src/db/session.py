from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.db.models import Base

TEST_DATABASE = 'test.db'

engine = create_engine('sqlite:///dev.db', connect_args={'check_same_thread':False})
test_engine = create_engine('sqlite:///' + TEST_DATABASE, connect_args={'check_same_thread':False})

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
TestSession = sessionmaker(bind=test_engine)