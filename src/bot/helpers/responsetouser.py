#Класс, описывающий торговое предложение
class ResponseCouponsSTREAM:

    def __init__(self, sale, company, price ):
        self.sale = sale
        self.company = company
        self.price = price

    def playresponse(self):
        return 'Акция {0}\nОт компании {1}\nЦена: {2}\n'\
        .format(self.sale, self.company, self.price)