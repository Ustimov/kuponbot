# coding=utf-8

# Класс, храняший параметры запроса пользователя
class UserRequestDTO:
    def __init__(self, category="ФастФуд", max_price = 1000, location="", add_description="", current_page_number=1,
                 coupons_per_page=50):
        self.category = category
        self.add_description = add_description  # для разделения kfc и burger king в категории фастфуд
        self.max_price = max_price
        self.location = location  # адрес - будет null
        self.current_page_number = current_page_number  # текущий номер листа при пагинации
        self.coupons_per_page = coupons_per_page # купонов на странице

    def set_category(self, categoty):
        self.category = categoty

    def set_max_price(self, max_price):
        self.max_price = max_price

    def set_location(self, location):
        self.location = location

    def set_add_description(self, add_description):
        self.add_description = add_description

    def set_current_page_number(self, current_page_number):
        self.current_page_number = current_page_number

    def get_category(self):
        return self.category

    def get_max_price(self):
        return self.max_price

    def get_location(self):
        return self.location

    def get_add_description(self):
        return self.add_description

    def get_current_page_number(self):
        return self.current_page_number

    def get_coupons_per_page(self):
        return self.coupons_per_page

    def playrequest(self):
        return 'Ваш запрос категории {0} с бюджетом {1} рублей в районе {2}' \
            .format(self.category, self.max_price, self.location)

