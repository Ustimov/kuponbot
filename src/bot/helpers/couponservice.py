# coding=utf-8

from src.db.models import Category, Coupon
from src.log.logger import get_logger

logger = get_logger(name='kuponService')


class CouponService:
    """Обработка запросов пользователя"""

    def __init__(self, session):
        self.session = session

    def get_coupons_by_category(self, user_request):
        logger.info('Пользователь сделал запрос {0}'.format(user_request.get_category()))
        choosecategory = self.session.query(Category).filter(Category.name == user_request.get_category()).first()
        coupons = self.session.query(Coupon).filter(Coupon.category == choosecategory)\
            .join(Category, Category.id == Coupon.category_id).all()
        logger.debug('Запрос выполнен')
        return coupons

    def get_coupons_by_category_and_max_price(self, user_request):
        category = self.session.query(Category).filter(Category.name == user_request.get_category()).first()
        limit = user_request.get_coupons_per_page()
        page = user_request.get_current_page_number()
        if not category:
            return None
        return self.session.query(Coupon).filter(Coupon.category == category,
                                                 Coupon.min_price <= user_request.get_max_price())\
            .offset(limit * (page - 1)).limit(limit).all()
