# coding=utf-8

import telebot
from telebot import types
import random
from src.bot.helpers.couponservice import CouponService
from src.bot.helpers.userrequest import UserRequestDTO
from src.config import TOKEN
from src.db.session import Session
from src.log.logger import get_logger

# Создаём logger
logger = get_logger()

bot = telebot.TeleBot(TOKEN)

session = Session()


userRequest = UserRequestDTO()
dic_user_request = {}
couponService = CouponService(session)
hide_reply = types.ReplyKeyboardRemove()


# Обработчик команд '/start' и '/help'.
@bot.message_handler(commands=['start', 'help'])
def handle_start_help(message):
    greeting_msg = 'Здравствуйте, {0}! КупонБот поможет тебе порадовать душу и живот приятными предложениями со ' \
                   'скидками.'.format(message.from_user.first_name)
    bot.send_message(message.chat.id, greeting_msg)
    step1_category_markup(message)


def step1_category_markup(message):
    category_markup = types.ReplyKeyboardMarkup()
    category_markup.row('Еда')
    category_markup.row('Развлечения')
    category_markup.row('ФастФуд')
    category_markup.row('Мне повезет :)')
    category_markup.row('Отмена')
    bot.send_message(message.chat.id, "Выберите категорию:", reply_markup=category_markup)


@bot.message_handler(regexp='Мне повезет')
def im_lucky(message):
    try: 
        userRequest = UserRequestDTO()
        categories = ['Еда', 'Развлечения', 'ФастФуд']
        rand_num = random.randint(1, 3)
        dic_user_request[message.chat.id] = userRequest
        dic_user_request[message.chat.id].set_category(categories[rand_num - 1])
        dic_user_request[message.chat.id].set_current_page_number(1)
        dic_user_request[message.chat.id].set_max_price('100000')
        coupons = couponService.get_coupons_by_category_and_max_price(dic_user_request[message.chat.id])
        if not coupons:
            bot.send_message(message.chat.id, 'Упс, не повезло :)', reply_markup=hide_reply)
            return
        i = 0
        for c in coupons:
            i += 1
            if i == rand_num:
                response_text = '{0}. {1}\n Ваш промокод:{2}\n Бюджет:  {3} \n\n'.format(
                    i, c.company.name, c.description, c.code, c.min_price)
    except Exception:
        logger.warn('Во время lucky возврата купонов произошла ошибка', Exception)
    else:
        bot.send_message(message.chat.id, response_text, reply_markup=hide_reply)
        logger.info('Пользователь {0} получил ответ {1}'.format(message.from_user.first_name, response_text))  
    
    
# Обработчик сообщений, подходящих под указанное регулярное выражение
@bot.message_handler(regexp='Отмена')
def cancel_handler(message):
    logger.info('Пользователь {0} отменил действие'.format(message.from_user.first_name))
    bot.send_message(message.chat.id, 'Действие отменено', reply_markup=hide_reply)
    dic_user_request.pop(message.chat.id, None)


# Обработчик сообщений, подходящих под указанное регулярное выражение
@bot.message_handler(regexp='Развлечения')
@bot.message_handler(regexp='Еда')
def step2_category_message(message):
    try:
        logger.info('Пользователь {0} сделал запрос'.format(message.from_user.first_name))
        category = message.text
        userRequest = UserRequestDTO()
        dic_user_request[message.chat.id] = userRequest
        dic_user_request[message.chat.id].set_category(category)
        logger.debug('Категория запроса {0}'.format(category))
        dic_user_request[message.chat.id].set_current_page_number(1)
        logger.debug('Запрос пользователя: категория {0}, доп.описание: {1}, страница пагинации {2}'.format(
            dic_user_request[message.chat.id].get_category(), dic_user_request[message.chat.id].get_add_description(),
            dic_user_request[message.chat.id].get_current_page_number()))
        step3_max_price_markup(message)
    except Exception:
        logger.warn('Во время выбора категории произошла ошибка', Exception)  


@bot.message_handler(regexp='ФастФуд')
def step2_fastfood_markup(message):
    try:
        logger.info('Пользователь {0} сделал запрос'.format(message.from_user.first_name))
        category = message.text
        logger.debug('Категория запроса {0}'.format(category))
        fastfood_markup = types.ReplyKeyboardMarkup()
        fastfood_markup.row('Burger King')
        fastfood_markup.row('KFC')
        fastfood_markup.row('Отмена')
        bot.send_message(message.chat.id, "Выберите заведение:", reply_markup=fastfood_markup)
    except Exception:
        logger.warn('Во время выбора категории произошла ошибка', Exception)


@bot.message_handler(regexp='Burger King')
@bot.message_handler(regexp='KFC')
def step2_fastfood_message(message):
    try:
        userRequest = UserRequestDTO()
        dic_user_request[message.chat.id] = userRequest
        dic_user_request[message.chat.id].set_category('ФастФуд')
        fastfood = message.text
        dic_user_request[message.chat.id].set_add_description(fastfood)
        logger.debug('Заведение {0}'.format(fastfood))
        dic_user_request[message.chat.id].set_current_page_number(1)
        step3_max_price_markup(message)    
    except Exception:
        logger.warn('Во время выбора фастфуда произошла ошибка', Exception)


def step3_max_price_markup(message):
    max_price_markup = types.ReplyKeyboardMarkup()
    max_price_markup.row('300')
    max_price_markup.row('500')
    max_price_markup.row('1000')
    max_price_markup.row('Я не считаю мелочь')
    max_price_markup.row('Отмена')
    bot.send_message(message.chat.id, "Введите бюджет:", reply_markup=max_price_markup)


@bot.message_handler(regexp='[1-9]\d*')
@bot.message_handler(regexp='Я не считаю мелочь')
def step3_add_max_price(message):
    if check_for_hacker(message):
        return
    try: 
        maxprice = message.text
        if maxprice != 'Я не считаю мелочь':
            dic_user_request[message.chat.id].set_max_price(maxprice)
        else:
            dic_user_request[message.chat.id].set_max_price('100000')   
        logger.debug('Бюджет {0}'.format(maxprice))
        step4_get_coupons(message)
    except Exception:
        logger.warn('Во время выбора бюджета произошла ошибка', Exception)       


def step4_get_coupons(message):
    try:
        logger.debug('Запрос пользователя: категория {0}, доп.описание: {1}, бюджет {2}, страница пагинации {3}'.format(
        dic_user_request[message.chat.id].get_category(), dic_user_request[message.chat.id].get_add_description(),
            dic_user_request[message.chat.id].get_max_price(),
        dic_user_request[message.chat.id].get_current_page_number()))
        user_request = dic_user_request[message.chat.id]
        coupons = couponService.get_coupons_by_category_and_max_price(user_request)
        if not coupons:
            bot.send_message(message.chat.id, 'Нет предложений на такой бюджет', reply_markup=hide_reply)
            return
        logger.debug('Возвращено {0} предложений'.format(coupons))
        response_text = ''
        description = dic_user_request[message.chat.id].get_add_description()
        i = 0
        for c in coupons:
            if description == c.company.name:
                i += 1
                response_text += '{0}. {1}. {2}: {3} \n\n'.format(i, c.company.name, c.description, c.code, c.min_price)
    except Exception:
        logger.warn('Во время возврата купонов произошла ошибка', Exception)
    else:
        bot.send_message(message.chat.id, response_text, reply_markup=hide_reply)
        dic_user_request.pop(message.chat.id, None)
        logger.info('Пользователь {0} получил ответ {1}'.format(message.from_user.first_name, response_text))    


def check_for_hacker(message):
    if not message.chat.id in dic_user_request:
        bot.send_photo(message.chat.id, open('src/bot/img/palehche.jpeg', 'rb'), reply_markup=hide_reply)
        handle_start_help(message)
        return True
    return False


# Основной цикл слушателя событий
if __name__ == '__main__':
    bot.polling(none_stop=True)