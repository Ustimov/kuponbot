# coding=utf-8

import os
import sqlite3
import unittest

from src.bot.helpers.couponservice import CouponService
from src.bot.helpers.userrequest import UserRequestDTO
from src.db.session import TestSession, TEST_DATABASE


class TestCouponService(unittest.TestCase):
    def setUp(self):
        if os.path.isfile(TEST_DATABASE):
            os.remove(TEST_DATABASE)
        with open('src/test/testdb.sql', 'r') as file:
            sql = file.read()
            db = sqlite3.connect(TEST_DATABASE)
            db.executescript(sql)
            db.close()
            self.session = TestSession()
            self.service = CouponService(self.session)

    def tearDown(self):
        self.session.close()

    def test_coupons_by_category_and_max_price(self):
        user_request1 = UserRequestDTO('ФастФуд', 100, '', '', 1, 2)
        coupons1 = self.service.get_coupons_by_category_and_max_price(user_request1)

        self.assertEqual(len(coupons1), 2)
        self.assertEqual(coupons1[0].description, 'Гамбургер + картофель фри(дж.) за 69 р.')
        self.assertEqual(coupons1[1].description, 'Второе капучино(станд.) бесплатано')

        user_request2 = UserRequestDTO('ФастФуд', 1000, '', '', 3, 10)
        coupons2 = self.service.get_coupons_by_category_and_max_price(user_request2)

        self.assertEqual(len(coupons2), 5)
        self.assertEqual(coupons2[0].description, 'Байтс станд.+ФРИ средний 139р вместо 167р')

    def test_get_coupons_by_category(self):
        coupons = self.service.get_coupons_by_category(UserRequestDTO('ФастФуд', 100, '', '', 1, 2))
        self.assertEqual(len(coupons), 25)

