import unittest
import os
import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..db.models import Base, Address, Company, Coupon, Category


class TestModels(unittest.TestCase):
    test_database_name = 'test.db'

    # установить соединение с sqlalchemy
    def setUp(self):
        if os.path.isfile(self.test_database_name):
            os.remove(self.test_database_name)
        engine = create_engine('sqlite:///' + self.test_database_name)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

    # оборвать сессию
    def tearDown(self):
        self.session.close()
        os.remove(self.test_database_name)

    # тест на добавление элементов в каждую из таблиц БД:
    # создаются фиктивные instance, превращаются в записи, записываются в таблицу,
    # вынимаются и сравниваются с изначальными instance
    def test_add(self):
        address = Address()
        self.session.add(address)
        self.session.commit()
        address_from_db = self.session.query(Address).first()
        self.assertTrue(address is address_from_db, 'Adding address test failed')

        company = Company(name="Company", address=address)
        self.session.add(company)
        self.session.commit()
        company_from_db = self.session.query(Company).first()
        self.assertTrue(company is company_from_db, 'Adding company test failed')

        coupon = Coupon(company=company, code='12345', description='coupon', start_datetime=datetime.datetime.now(),
                        expire_datetime=datetime.datetime.now())
        self.session.add(coupon)
        self.session.commit()
        coupon_from_db = self.session.query(Coupon).first()
        self.assertTrue(coupon is coupon_from_db, 'Adding coupon test failed')

        category = Category(name="Category")
        self.session.add(category)
        self.session.commit()
        category_from_db = self.session.query(Category).first()
        self.assertTrue(category is category_from_db, 'Adding category test failed')

