# coding=utf-8

import unittest
from ..bot.helpers.userrequest import UserRequestDTO


class TestUserRequest(unittest.TestCase):

    def test_print(self):
        page = 1
        userrequest = UserRequestDTO('Еда', 1500, 'м.Бауманская', 'описание', 1, 2)
        self.assertEqual('Ваш запрос категории Еда с бюджетом 1500 рублей в районе м.Бауманская',
                         userrequest.playrequest())