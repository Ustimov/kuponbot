BEGIN TRANSACTION;
CREATE TABLE users (
	id INTEGER NOT NULL,
	username VARCHAR(32) NOT NULL,
	password VARCHAR(88) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE coupons (
	id INTEGER NOT NULL,
	category_id INTEGER,
	company_id INTEGER,
	code VARCHAR(255) NOT NULL,
	description VARCHAR(1000) NOT NULL,
	min_price INTEGER,
	start_datetime DATETIME,
	expire_datetime DATETIME,
	PRIMARY KEY (id),
	FOREIGN KEY(category_id) REFERENCES categories (id),
	FOREIGN KEY(company_id) REFERENCES companies (id)
);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (1,2,1,'https://goo.gl/elEpu3','Скидка 1000 руб. на прохождение онлайн фитнес курса!',5900,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (2,2,2,'https://goo.gl/cWVklr','Квесты и развлечения: Вдвоем дешевле! Скидка до 50%!',1000,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (3,2,2,'https://goo.gl/7t6FWV','При оплате любого приключения онлайн получаешь скидку 500 рублей!',1000,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (4,2,3,'https://goo.gl/LqfFeG','Скидка до 50% на картины на холсте!',2000,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (5,1,4,'https://goo.gl/7n9Eqe','Скидка 300 рублей на первый заказ',1000,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (6,1,4,'https://goo.gl/yzuhUZ','Скидка 30% на первый заказ!',1000,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (7,1,5,'Promokodex, promokodex, promokodik','Скидка 500 рублей на первый заказ',2200,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (8,1,6,'https://goo.gl/67Zt2h','500 баллов в подарок!',500,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (9,1,6,'https://goo.gl/3OTsy2','Бесплатная доставка заказов на сумму от 300 руб',300,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (10,3,7,'9806','Гамбургер + картофель фри(дж.) за 69 р.',69,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (11,3,7,'9797','Второе капучино(станд.) бесплатано',69,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (12,3,7,'9903','Луковые колечки 9шт. за 69 р',69,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (13,3,7,'9869','Сырный Джо + Чизбургер бесплатно',150,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (14,3,7,'9873','Биг Кинг за 99р',99,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (15,3,7,'9878','Пирожок бесплатно к Воппер',150,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (16,3,7,'9775','Чизбургер бесплатно к Стейкхаус',150,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (17,3,7,'9770','Второй Лонг Чикен бесплатно',99,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (18,3,7,'9764','Молочный коктейль за 35р',35,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (19,3,7,'9783','Брауни с мороженым и Капучино за 139р',139,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (20,3,7,'9763','Нагетсы 5 шт и соус за 79р',79,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (21,3,7,'9774','Второе САНДЕЙ бесплатно',99,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (22,3,7,'9277','Кинг Фри 2 шт за 99р',99,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (23,3,7,'9786','Два Пирожка и два Капучино за 199р',199,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (24,3,8,'9050','Баскет 16 крыльев+2 пепси 0,5 399р вместо 497р',399,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (25,3,8,'9051','Баскет 25 крыльев + 2 ФРИ малый 499р вместо 583р',499,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (26,3,8,'9053','6 крыльев + соус 155р вместо 190р',155,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (27,3,8,'9055','Боксмастер ор./остр. ФРИ малый 169р вместо 214р',169,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (28,3,8,'9056','Боксмастер ор./остр. ФРИ малый 169р вместо 214р',169,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (29,3,8,'9057','Сандерс+ФРИ средний 115р вместо 139р',115,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (30,3,8,'9058','Байтс станд.+ФРИ средний 139р вместо 167р',139,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (31,3,8,'9059','Сандерс+ПЕПСИ 0,4+ФРИ малый +2 СТРИПСА ор./остр 199р вместо 253р',199,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (32,3,8,'9063','БОКСМАСТЕР ор./ост.+ПЕПСИ 0,4+ФРИ малый+2 СТРИПСАор./остр 269р вместо 343р',269,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (33,3,8,'9065','Сандерс + пепси 0.4 +ФРИ малый +3 крыла 199р вместо 279р',199,NULL,NULL);
INSERT INTO `coupons` (id,category_id,company_id,code,description,min_price,start_datetime,expire_datetime) VALUES (34,3,8,'9069','Боксмастер ор./ост.+ Пепси 0.4+ФРИ малый +3 крыла 269р вместо 369р',269,NULL,NULL);
CREATE TABLE companies (
	id INTEGER NOT NULL,
	name VARCHAR(255) NOT NULL,
	address_id INTEGER,
	phone_number VARCHAR(255),
	PRIMARY KEY (id),
	FOREIGN KEY(address_id) REFERENCES addresses (id)
);
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (1,'Upgrade-ra',NULL,'8 (800) 775 81 27');
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (2,'EXIT GAMES',2,'8 (499) 638 22 65');
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (3,'ProCanvas',NULL,'8 (495) 646 13 90');
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (4,'Foodfox',NULL,'8 (495) 120 25 07');
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (5,'Партия Еды',NULL,'8 (495) 268 07 28');
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (6,'Delivery Club',NULL,NULL);
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (7,'Burger King',NULL,NULL);
INSERT INTO `companies` (id,name,address_id,phone_number) VALUES (8,'KFC',NULL,NULL);
CREATE TABLE categories (
	id INTEGER NOT NULL,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO `categories` (id,name) VALUES (1,'Еда');
INSERT INTO `categories` (id,name) VALUES (2,'Развлечения и  услуги');
INSERT INTO `categories` (id,name) VALUES (3,'ФастФуд');
CREATE TABLE addresses (
	id INTEGER NOT NULL,
	metro_station VARCHAR(255),
	"distinct" VARCHAR(255),
	full_adress VARCHAR(255),
	PRIMARY KEY (id)
);
INSERT INTO `addresses` (id,metro_station,'distinct',full_adress) VALUES (1,'Бауманская','ЦАО',NULL);
INSERT INTO `addresses` (id,metro_station,'distinct',full_adress) VALUES (2,'Курская','ЦАО',NULL);
COMMIT;
