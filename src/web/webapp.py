# coding=utf-8

from flask import Flask, render_template, request, redirect, url_for
from flask_login import LoginManager, login_user, logout_user, login_required
from passlib.hash import pbkdf2_sha256
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.db.models import Base, Address, Category, Company, Coupon, User

engine = create_engine('sqlite:///dev.db')
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)
app.secret_key = "very_secret_key"

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(id):
    return session.query(User).filter_by(id=id).first()


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    username = request.form['username']
    password = request.form['password']
    user = session.query(User).filter_by(username=username).first()
    if not user:
        return render_template('login.html', error='Пользователь с таким именем не найден')
    hash = user.password
    ok = pbkdf2_sha256.verify(password, hash)
    if not ok:
        return render_template('login.html', error='Неверно указан пароль')
    login_user(user)
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')


# Список адресов
@app.route('/address', methods=['GET'])
@login_required
def address_list():
    return render_template('address_list.html', addresses=session.query(Address).all())


# Детальное представление и удаление адреса
@app.route('/address/<int:id>', methods=['GET', 'POST'])
@login_required
def address(id):
    if request.method == 'GET':
        return render_template('address_detail.html', address=session.query(Address).filter_by(id=id).first())
    session.query(Address).filter_by(id=id).delete()
    session.commit()
    return redirect(url_for('address_list'))


# Добавление адреса
@app.route('/address/add', methods=['GET', 'POST'])
@login_required
def address_add():
    if request.method == 'POST':
        metro_station = request.form['metro_station']
        session.add(Address(metro_station=metro_station))
        session.commit()
    return render_template('address_add.html')


# Список категорий
@app.route('/category', methods=['GET'])
@login_required
def category_list():
    return render_template('category_list.html', categories=session.query(Category).all())


# Детальное представление и удаление категории
@app.route('/category/<int:id>', methods=['GET', 'POST'])
@login_required
def category(id):
    if request.method == 'GET':
        return render_template('category_detail.html', category=session.query(Category).filter_by(id=id).first())
    session.query(Category).filter_by(id=id).delete()
    session.commit()
    return redirect(url_for('category_list'))


# Добавление категории
@app.route('/category/add', methods=['GET', 'POST'])
@login_required
def category_add():
    if request.method == 'POST':
        name = request.form['name']
        session.add(Category(name=name))
        session.commit()
    return render_template('category_add.html')


# Список компаний
@app.route('/company', methods=['GET'])
@login_required
def company_list():
    return render_template('company_list.html', companies=session.query(Company).all())


# Детальное представление и удаление компании
@app.route('/company/<int:id>', methods=['GET', 'POST'])
@login_required
def company(id):
    if request.method == 'GET':
        return render_template('company_detail.html', company=session.query(Company).filter_by(id=id).first())
    session.query(Company).filter_by(id=id).delete()
    session.commit()
    return redirect(url_for('company_list'))


# Добавление компании
@app.route('/company/add', methods=['GET', 'POST'])
@login_required
def company_add():
    if request.method == 'POST':
        name = request.form['name']
        session.add(Company(name=name))
        session.commit()
    return render_template('company_add.html')


# Список купонов
@app.route('/coupon', methods=['GET'])
@login_required
def coupon_list():
    return render_template('coupon_list.html', coupons=session.query(Coupon).all())


# Детальное представление и удаление купона
@app.route('/coupon/<int:id>', methods=['GET', 'POST'])
@login_required
def coupon(id):
    if request.method == 'GET':
        return render_template('coupon_detail.html', coupon=session.query(Coupon).filter_by(id=id).first())
    session.query(Coupon).filter_by(id=id).delete()
    session.commit()
    return redirect(url_for('coupon_list'))


# Добавление купона
@app.route('/coupon/add', methods=['GET', 'POST'])
@login_required
def coupon_add():
    if request.method == 'POST':
        code = request.form['code']
        description = request.form['description']
        session.add(Coupon(code=code, description=description))
        session.commit()
    return render_template('coupon_add.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
