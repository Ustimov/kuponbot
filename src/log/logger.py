import logging.config


def get_logger(config='conf/logging.conf', name='kuponbot'):
    logging.config.fileConfig(config)
    return logging.getLogger(name)